import com.devcamp.Circle;
import com.devcamp.Cylinder;

public class App {
    public static void main(String[] args) throws Exception {
        //task 4
        Circle circle1 = new Circle();
        Circle circle2 = new Circle(2.0);
        Circle circle3 = new Circle(3.0, "green");
        System.out.println("Hinh tron 1: " + circle1);
        System.out.println("Hinh tron 2: " + circle2);
        System.out.println("Hinh tron 3: " + circle3);
        System.out.println("Dien tich hinh tron 1: " + circle1.getArea());
        System.out.println("Dien tich hinh tron 2: " + circle2.getArea());
        System.out.println("Dien tich hinh tron 3: " + circle3.getArea());
        //task 5
        Cylinder cylinder1 = new Cylinder();
        Cylinder cylinder2 = new Cylinder(2.5);
        Cylinder cylinder3 = new Cylinder(3.5, 1.5);
        Cylinder cylinder4 = new Cylinder(3.5, "green", 1.5);
        System.out.println("Hinh tru 1: " + cylinder1);
        System.out.println("Hinh tru 2: " + cylinder2);
        System.out.println("Hinh tru 3: " + cylinder3);
        System.out.println("Hinh tru 4: " + cylinder4);
        System.out.println("The tich hinh tru 1: " + cylinder1.getVolume());
        System.out.println("The tich hinh tru 2: " + cylinder2.getVolume());
        System.out.println("The tich hinh tru 3: " + cylinder3.getVolume());
        System.out.println("The tich hinh tru 4: " + cylinder4.getVolume());
    }
}
